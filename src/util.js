const { interfaces } = require("mocha")



function checkAge(age){

    if(typeof(age) !== 'number'){
        return "Not a number"
    }
    if(age == ''){
        return "Error: Age is empty!"
    }
    if(isNaN(age)){
        return "Error: Age should be defined!"
    }
    if(age === null){
        return "Error: Age should not be null!"
    }
    if(age.length == 0){
        return "Error: Age should not be 0!"
    }

}

function checkFullName(fullName){

    if(fullName == ''){
        return "Error: fullName is empty!"
    }
    if(fullName === null){
        return "Error: fullName should not be null!"
    }
    if(fullName === undefined){
        return "Error: fullName should be defined!"
    }
    if(typeof(fullName) != 'string'){
        return "Error: fullName should be a string!"
    }
    if(fullName.length == 0){
        return "Error: fullname should not be 0!"
    }
}

module.exports = {
    checkAge: checkAge,
    checkFullName: checkFullName
}