const {checkAge, checkFullName} = require('../src/util.js');
const {assert, expect} = require('chai');

describe('test_age', () => {
    it('age_not_integer', () => {
        const age = 10;
        assert.strictEqual(typeof checkAge(age), 'number', 'not a number');
    });

    it('age_not_empty', () => {
        const age ="";
        assert.isNotEmpty(checkAge(age)); 
        
    });

    it('age_not_undefined', () => {
        const age = undefined;
        assert.isDefined(checkAge(age));
    });

    it('age_not_null', () => {
        const age = null;
        assert.isNotNull(checkAge(age));
        
    });

    it('age_not_zero', () => {
        const age = 0;
        assert.isNotEmpty(checkAge(age));
    });

});

describe('test_fullName', () => {
    it('fullName_not_empty', () => {
        const fullName ="";
        assert.isNotEmpty(checkFullName(fullName));         
    });

    it('fullName_not_null', () => {
        const fullName = null;
        assert.isNotNull(checkFullName(fullName));
    });

    it('fullName_not_undefined', () => {
        const fullName = undefined;
        assert.isDefined(checkFullName(fullName));
    });

    it('fullName_is_string_type', () => {
        const fullName = "John";
        assert.isNotString(checkFullName(fullName));
    });

    it('fullName_not_zero', () => {
        const fullName = 0;
        assert.isNotEmpty(checkFullName(fullName));
    });

})
